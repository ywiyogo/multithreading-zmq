// https://zeromq.org/get-started/?language=c&library=libzmq#

#include "zmq_guide.h"
#include <zmq.h>
#include <assert.h>

int Server::start(int max, std::string name)
{
    //  Socket to talk to clients
    void *context = zmq_ctx_new();
    void *responder = zmq_socket(context, ZMQ_REP);
    int rc = zmq_bind(responder, "tcp://*:5555");
    assert(rc == 0);

    for (auto counter = 0; counter < max; ++counter)
    {
        char buffer[10];
        zmq_recv(responder, buffer, 10, 0);
        printf("  %s: Received Hello\n", name.c_str());
        sleep(1); //  Do some 'work'
        zmq_send(responder, "World", 5, 0);
    }
    printf("Server %s stopped\n", name.c_str());
    return 0;
}

int Client::start(int msg_num, std::string name)
{
    printf("Connecting to hello world server…\n");
    void *context = zmq_ctx_new();
    void *requester = zmq_socket(context, ZMQ_REQ);
    zmq_connect(requester, "tcp://localhost:5555");

    int request_nbr;
    for (request_nbr = 0; request_nbr != msg_num; request_nbr++)
    {
        char buffer[msg_num];
        printf("%s: Sending Hello %d…\n", name.c_str(), request_nbr);
        zmq_send(requester, "Hello", 5, 0);
        zmq_recv(requester, buffer, msg_num, 0);
        printf("     %s: Received World %d\n\n", name.c_str(), request_nbr);
    }
    zmq_close(requester);
    zmq_ctx_destroy(context);
    printf("Client %s stopped\n", name.c_str());
    return 0;
}