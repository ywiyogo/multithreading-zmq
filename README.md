# Multithreading Test of ZMQ

## Getting Started

Starting 4 parallel jobs:

```
mkdir build && cd build
cmake ..
ctest --output-on-failure -j4 -V
```

