#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <string>

class Server
{
public:
    static int start(int max, std::string name);
};

class Client
{
public:
    static int start(int msg_num, std::string name);
};