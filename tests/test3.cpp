#include "gtest/gtest.h"
#include "zmq_guide.h"
#include<thread>

TEST(zmq, test3) {
    std::thread t1(Server::start, 10, "Test3");
    std::thread t2(Client::start, 10, "Test3");
    t1.join();
    t2.join();
}